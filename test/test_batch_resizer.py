#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Tests for `batch_resizer` module.
"""
import pytest
from batch_resizer import batch_resizer


class TestBatch_resizer(object):

    @classmethod
    def setup_class(cls):
        pass

    @pytest.mark.parametrize("ipt, exp", [
        (0.5, '50%'),
        (0.05, '5%')
    ])
    def test_gen_storage_dir_name(self, ipt, exp):
        assert batch_resizer.gen_storage_dir_name(ipt) == exp

    @pytest.mark.parametrize("ipt, exp", [
        (0.5, True),
        (0.05, True),
        (1.01, False),
        (-1.01, False),
    ])
    def test_validate_input(self, ipt, exp):
        lower, upper = 0, 1
        assert batch_resizer.validate_percentage(ipt, lower, upper) == exp

    @classmethod
    def teardown_class(cls):
        pass
