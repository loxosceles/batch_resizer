.. :changelog:

History
-------

0.1.0 (19/07/2020)
++++++++++++++++++

* First release on PyPI.
