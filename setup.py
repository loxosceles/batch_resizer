#!/usr/bin/env python

import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

if sys.argv[-1] == 'build_dist':
    os.system('pyinstaller --onefile ./batch_resizer/batch_resizer.py')
    sys.exit()

readme = open('README.rst').read()
doclink = """
Documentation
-------------

The full documentation is at http://batch_resizer.rtfd.org."""
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='batch_resizer',
    version='0.1.0',
    description='Resize images in folders intelligently',
    long_description=readme + '\n\n' + doclink + '\n\n' + history,
    author='Magnus "Loxosceles" Henkel',
    author_email='loxosceles@gmx.de',
    url='https://gitlab.com/loxosceles/batch_resizer',
    packages=[
        'batch_resizer',
    ],
    package_dir={'batch_resizer': 'batch_resizer'},
    include_package_data=True,
    install_requires=[
    ],
    license='MIT',
    zip_safe=False,
    keywords='batch_resizer',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
)
