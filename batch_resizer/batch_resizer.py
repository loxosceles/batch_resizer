#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Batch Resizer

    batch resize all jpg/png files in the current directory and write a resized copy
    into a seperate folder inside that directory.

    ARGS: percentage to which the image will be resized.
"""

from PIL import Image
import glob
import os
import sys
import math
import argparse

VALID_EXTENSIONS = ['jpg', 'JPG', 'JPEG', 'jpeg', 'png', 'PNG']

def resize_img(percentage):

    image_files = []

    for imgfile in map(lambda x: '*.' + x, VALID_EXTENSIONS):
        image_files.extend(glob.glob(imgfile))

    for infile in image_files:
        file_name, ext = os.path.splitext(infile)
        img = Image.open(infile)
        exif_data = img.info['exif']

        w, h = float(img.width), float(img.height)

        # rotate if necessary
        img = rotate(img)

        # calculate basewidth from percentage
        # size: The requested size in pixels, as a 2-tuple: (width, height)
        # if height bigger than width we have portrait
        if w < h:
            print("portrait")
            height = h * math.sqrt(percentage)
            width = (w / h) * height
            print(width, height)
        # if width bigger than height we have landscape
        else:
            print('landscape')
            width = w * math.sqrt(percentage)
            height = (h / w) * width
            print(width, height)

        print("Printing image size")
        print(f'width: {w}')
        print(f'height: {h}')

        img = img.resize((int(width), int(height)))
        resolution = str(width).split('.')[0] + "x" + str(height).split('.')[0]
        save_dir = gen_storage_dir_name(percentage)

        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)

        img.save(f'{save_dir}/{file_name}_{resolution}{ext}', "JPEG", exif=exif_data)

        print(f'Resizing {file_name}')

def gen_storage_dir_name(percentage):
    return f'{int(100 * percentage)}%'

def rotate(img):
    print("Rotating image")
    try:
        orientation = 0x0112
        exif = img._getexif()
        if exif is not None:
            orientation = exif[orientation]
            rotations = {
                3: Image.ROTATE_180,
                6: Image.ROTATE_270,
                8: Image.ROTATE_90
            }
            if orientation in rotations:
                img.transpose(rotations[orientation])
    finally:
        return img

def calc_height(orig_width, orig_height):
    temp = (int(w_str) / float(orig_width))
    return int((float(orig_height) * temp))

def validate_percentage(v, lower, upper):
    return lower <= v <= upper


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Scale image inside folder by percentage')

    # Add command line arguments, only the path is obligatory
    parser.add_argument('-p', '--percentage',
                        help='Percentage by which the images will be scaled')

    args = vars(parser.parse_args())

    # If keyword is not given as argument ask for it
    if args['percentage']:
        percent = float(args['percentage'])
    else:
        percent = float(input("Percentage of scaling: "))

    # Make sure percent is between 0.01 and 0.99
    if not validate_percentage(percent, 0.01, 0.99):
        print("\nPercent has to be given as a decimal between 0.01 and 0.99\n")
        sys.exit(0)

    resize_img(percent)
